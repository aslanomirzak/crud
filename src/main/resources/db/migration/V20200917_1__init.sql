drop table if exists auth CASCADE;
create table "auth"
(
	id serial not null,
	login varchar default 255 not null,
	email varchar default 255 not null,
	password varchar default 128 not null,
	role varchar default 255 not null,
	forgot_password_key varchar default 128,
	forgot_password_key_timestamp bigint,
	company_unit_id bigint
);

create unique index auth_id_uindex
	on "auth" (id);

alter table "auth"
	add constraint auth_pk
		primary key (id);


drop table if exists share CASCADE;
create table "share"
(
	id bigserial not null,
	request_id int8,
	note varchar default 255,
	sender_id int8,
	receiver_id int8,
	share_timestamp int8
);

create unique index share_id_uindex
	on share (id);

alter table share
	add constraint share_pk
		primary key (id);



drop table if exists catalog CASCADE;
create table catalog
(
	id bigserial not null,
	name_ru varchar default 128,
	name_kz varchar default 128,
	name_en varchar default 128,
	parent_id int8,
	company_unit_id int8,
	created_timestamp int8,
	created_by int8,
	updated_timestamp int8,
	updated_by int8
);

create unique index catalog_id_uindex
	on catalog (id);

alter table catalog
	add constraint catalog_pk
		primary key (id);

