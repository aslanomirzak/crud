package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Share;
import kz.aitu.crud.repository.ShareRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShareService {
    private final ShareRepository shareRepository;

    public ShareService(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }

    public List<Share> getAll(){
        return (List<Share>) shareRepository.findAll();
    }

    public Share getById(long id){
        return shareRepository.findById(id);
    }

    public void deleteById(long id){
        shareRepository.deleteById(id);
    }

    public Share createShare(Share share){
        return shareRepository.save(share);
    }
}
