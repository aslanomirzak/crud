package kz.aitu.crud.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "share")
public class Share {
    @Id
    private long id;
    private long requestId;
    private String note;
    private long senderId;
    private long receiverId;
    private long shareTimestamp;
}
