package kz.aitu.crud.controller;


import kz.aitu.crud.model.Catalog;
import kz.aitu.crud.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping("api/catalog/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(catalogService.getById(id));
    }

    @GetMapping("api/catalog")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(catalogService.getAll());
    }

    @DeleteMapping("api/catalog/{id}")
    public void deleteById(@PathVariable long id){
        catalogService.deleteById(id);
    }

    @PostMapping("api/catalog")
    public ResponseEntity<?> createCatalog(@RequestBody Catalog catalog){
        return ResponseEntity.ok(catalogService.createCatalog(catalog));
    }
}
