package kz.aitu.crud.controller;

import kz.aitu.crud.model.Share;
import kz.aitu.crud.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareController {
    private final ShareService shareService;

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }

    @GetMapping("api/share/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(shareService.getById(id));
    }

    @GetMapping("api/share")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(shareService.getAll());
    }

    @DeleteMapping("api/share/{id}")
    public void deleteById(@PathVariable long id){
        shareService.deleteById(id);
    }

    @PostMapping("api/share")
    public ResponseEntity<?> createShare(@RequestBody Share share){
        return ResponseEntity.ok(shareService.createShare(share));
    }
}
