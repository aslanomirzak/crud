package kz.aitu.crud.controller;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController {
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("api/auth/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(authService.getById(id));
    }

    @GetMapping("api/auth")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(authService.getAll());
    }

    @DeleteMapping("api/auth/{id}")
    public void deleteById(@PathVariable long id){
        authService.deleteById(id);
    }

    @PostMapping("api/auth")
    public ResponseEntity<?> createAuth(@RequestBody Auth auth){
        return ResponseEntity.ok(authService.createAuth(auth));
    }
}
