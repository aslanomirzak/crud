package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.repository.AuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthService {
    private final AuthRepository authRepository;

    public AuthService(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    public List<Auth> getAll(){
        return (List<Auth>) authRepository.findAll();
    }

    public Auth getById(long id){
        return authRepository.findById(id);
    }

    public void deleteById(long id){
        authRepository.deleteById(id);
    }

    public Auth createAuth(Auth auth){
        return authRepository.save(auth);
    }
}
